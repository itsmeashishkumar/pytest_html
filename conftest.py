import sys
import pytest
import argparse
import platform
from _pytest.terminal import TerminalReporter
from pytest import mark
from selenium import webdriver
import logging
import os
import subprocess
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver
from webdriver_manager.microsoft import IEDriverManager
from helper.utils import *
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
import platform
import crayons

my_dict={}
pl = os_type()

def pytest_addoption(parser):
    parser.addoption('--count', default=1, type='int', metavar='count',
                     help='Run each test the specified number of times')
    parser.addoption("--browser", action="store", default="Chrome", help="Type in browser name e.g.chrome OR firefox")
    parser.addini('win_chrome_version', help='Version of Chrome driver in Windows')
    parser.addini('win_firefox_version', help='Version of Firefox driver in Windows')
    parser.addini('win_ie_version', help='Version of Internet Explorer or IE driver in Windows')
    parser.addini('mac_chrome_version', help='Version of Chrome driver in Mac')
    parser.addini('mac_firefox_version', help='Version of Firefox driver in Mac')

@pytest.fixture(scope="session", autouse=True)
def install_drivers(request):
    browser = request.config.getoption("--browser")
    win_chrome_version = request.config.getini("win_chrome_version")
    win_firefox_version = request.config.getini("win_firefox_version")
    win_ie_version = request.config.getini("win_ie_version")
    mac_chrome_version = request.config.getini("mac_chrome_version")
    mac_firefox_version = request.config.getini("mac_firefox_version")

    print("\n", "       ", crayons.magenta('\033[4m'+"                                                                "+"\033[0m"+"\n"))
    print("         "+crayons.magenta("Running Test in Machine with OS: \""+pl.upper()+"\" & BROWSER: \"" +browser +"\"....", bold=True))
    print("         "+crayons.magenta('\033[4m' + "                                                                " + "\033[0m"))

    if "win" in pl:
        if browser == "Chrome":
            executable_path=ChromeDriverManager(win_chrome_version).install()
            my_dict['executable_path']=executable_path
        elif browser == "Firefox":
            executable_path =GeckoDriverManager(win_firefox_version).install()
            my_dict['executable_path'] = executable_path
        elif browser =="IE":
            executable_path =  IEDriverManager(win_ie_version).install()
            my_dict['executable_path'] = executable_path
        else:
            executable_path = ChromeDriverManager(win_chrome_version).install()
            my_dict['executable_path'] = executable_path
    elif "mac" in pl:
        if browser == "Chrome":
            executable_path=ChromeDriverManager(mac_chrome_version).install()
            my_dict['executable_path']=executable_path
        elif browser == "Firefox":
            executable_path =GeckoDriverManager(mac_firefox_version).install()
            my_dict['executable_path'] = executable_path
        elif browser == "Safari":
            #In Mac by default safari driver is at "/usr/bin/safaridriver" so no need to install
            my_dict['executable_path']="/usr/bin/safaridriver"
        else:
            executable_path = ChromeDriverManager(mac_chrome_version).install()
            my_dict['executable_path'] = executable_path

@pytest.fixture(scope='class')
def test_setup(request):
    browser = request.config.getoption("--browser")

    if "win" in pl:
        if browser == "Chrome":
                driver = webdriver.Chrome(executable_path=my_dict['executable_path'])
        elif browser == "Firefox":
                driver = webdriver.Firefox(executable_path=my_dict['executable_path'])
        elif browser == "IE":
            driver = webdriver.Ie(executable_path=my_dict['executable_path'])
        else:
            driver = webdriver.Chrome(executable_path=my_dict['executable_path'])
    elif "mac" in pl:
        if browser == "Chrome":
            driver = webdriver.Chrome(executable_path=my_dict['executable_path'])
        elif browser == "Firefox":
            driver = webdriver.Firefox(executable_path=my_dict['executable_path'])
        elif browser == "Safari":
             driver = webdriver.Safari(executable_path=my_dict['executable_path'])
        else:
            driver = webdriver.Chrome(executable_path=my_dict['executable_path'])

    driver.implicitly_wait(10)
    driver.maximize_window()
    request.cls.driver = driver
    yield
    driver.quit()

"""This function runs the test count times, where count is the integer value provided in the pytest.ini"""
def pytest_collection_modifyitems(session, config, items):
    count = config.option.count
    items[:] = items * count


# If we want report saved for all instance then we can use or configure below code to get report with dynamic name or
# if we want report with same name we can just pass addopts = --html=abcreport.html inside pytest.ini so that
# next time we won't require to provide path name to save the report of that instance
# default_html_path = 'my_report.html'
# @pytest.hookimpl(tryfirst=True)
# def pytest_configure(config):
#     if not config.option.htmlpath:
#         config.option.htmlpath = default_html_path



