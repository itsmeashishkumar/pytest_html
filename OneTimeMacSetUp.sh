#!/usr/bin/env bash
OS=${SHELL}
echo $OS
if [ $OS == "/bin/bash" ]
then
    pip install --upgrade pip
    pip install pipenv
    scriptdir=`dirname $0`
    cd $scriptdir
    pipenv sync
    pipenv run python -m pytest
else [ $OS == "Windows" ]
    echo "script to run on Windows-Need to be configured"
fi




#NOTES
#1. "pipenv install --ignore-pipfile" is nearly equivalent to "pipenv sync"
    # A. pipenv sync [OPTIONS]
    #Installs all packages specified in Pipfile.lock.
    #B. pipenv install [OPTIONS] [PACKAGES]...
    # Installs provided packages and adds them to Pipfile, or (if no packages are given), installs all packages from Pipfile.
#Conclusion: It's better to use sync insteas install
#2.
